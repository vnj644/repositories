package Ex3;

public class App {
    public static void main(String[] args) {
      System.out.println("Сумма N натуральных чисел равна: " + summa(100));
    }

    public static int summa(int n){
      int result = ((1 + n) * n) / 2;
      return  result;
    }
}
