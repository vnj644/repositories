/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package Ex5;

public class App {
  public static void main(String[] args) {
      String str = "Hello World";
      System.out.println(countChar(str,'d'));
  }

  public static int countChar(String str, char n){
      int result = 0;
      char[] array = str.toCharArray();
      for (int i = 0; i < array.length; i++) {
          if (n == array[i]) {
              result++;
          }
      }
      return result;
  }
}
