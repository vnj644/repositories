package Task4;

public class Triangle extends Polygon{

    Triangle(int[] sides) throws NotTriangleException{
        super(sides);
        if(sides.length != 3){
            this.sides = null;
            this.perimeter = 0;
            throw new NotTriangleException("Это не треугольник!");
        }
    }

    public String output(){
        if(this.perimeter == 0){
            return "Это не треугольник!";
        }
        return "Я треугольник с периметром " + this.perimeter;
    }
}
