package Task4;

public class Operations {
    public PublicInterface[] allPolygons;

    Operations(PublicInterface[] allPolygons){
        this.allPolygons = allPolygons;
    }

    public void printAllObjects(){
        for (int i = 0; i < this.allPolygons.length; i++) {
            System.out.println(this.allPolygons[i].output());
        }
    }
}
