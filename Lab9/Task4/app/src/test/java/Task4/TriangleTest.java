package Task4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TriangleTest{

  @Test void trianglePerimeterTest() throws NotTriangleException{
    int[] a = new int[]{1,1,1};
    Triangle b = new Triangle(a);
    assertEquals(3, b.polygonPerimeter());
  }

  @Test void toStringTriangle() throws NotTriangleException{
    int[] a = new int[]{1,1,1};
    Triangle b = new Triangle(a);
    assertEquals("Я треугольник с периметром 3", b.output());
  }

  @Test void ExceptionTest() throws NotTriangleException{
    int[] a = new int[]{1,1};
    NotTriangleException exception = assertThrows(NotTriangleException.class, () -> new Triangle(a));
    assertEquals("Это не треугольник!", exception.getMassage());
  }
}
