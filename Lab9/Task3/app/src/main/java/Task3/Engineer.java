package Task3;

public class Engineer extends Employee {
    public Engineer(String name, int salary){
        super(name,salary);
        this.setPosition("Engineer");
    }

    public String toString(){
        return "Engineer [engineerName = " + getName()
                + ", engineerSalary = " + getSalary()
                + ", engineerPosition = " + getPosition() + "]";
    }

    public String engineer(){
        return "Я здесь все чиню";
    }

    public boolean equals(Object o){
        super.equals(o);
        Engineer engineer = (Engineer) o;
        if (engineer.getName() == this.getName() && engineer.getSalary() == this.getSalary() && engineer.getPosition() == this.getPosition()){
            return true;
        }
        return false;
    }
}
