package Task3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class EngineerTest{

  @Test void toStringTest(){
    Engineer a = new Engineer("Vladimir", 10000);
    assertEquals("Engineer [engineerName = Vladimir, engineerSalary = 10000, engineerPosition = Engineer]", a.toString());
  }

  @Test void engineerTest(){
    Engineer a = new Engineer("Jeka", 10000);
    assertEquals("Я здесь все чиню", a.engineer());
  }
}
