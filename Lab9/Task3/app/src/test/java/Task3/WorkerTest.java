package Task3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class WorkerTest{

  @Test void toStringTest(){
      Worker a = new Worker("Bob", 10000);
      assertEquals("Worker [workerName = Bob, workerSalary = 10000, workerPosition = Worker]", a.toString());
  }

  @Test void workerTest(){
    Worker a = new Worker("Maksim", 10000);
    assertEquals("Я рабочий...", a.worker());
  }
}
