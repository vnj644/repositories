package Task1;

public class Operations {
    public static int[][] matrixSum(Matrix m1, Matrix m2) throws NullPointerException, MatrixLengthException {
        int[][] result;
        int[][] matrix1 = m1.getMatrix();
        int[][] matrix2 = m2.getMatrix();
        if (matrix1 == null || matrix2 == null) {
            throw new NullPointerException("Матрицы пустые");
        }
        if (matrix1.length != matrix2.length || matrix1[0].length != matrix2[0].length) {
            throw new MatrixLengthException("Матрицы не одинаковые");
        }
        result = new int[matrix1.length][matrix1[0].length];
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix1[0].length; j++) {
                result[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }
        return result;
    }


    public static int[][] matrixSubtraction(Matrix mat1, Matrix mat2) throws NullPointerException, MatrixLengthException {
        int[][] result;
        int[][] m1 = mat1.getMatrix();
        int[][] m2 = mat2.getMatrix();
        if (m1 == null || m2 == null) {
            throw new NullPointerException("Матрицы пустые");
        }
        if (m1.length != m2.length || m1[0].length != m2[0].length) {
            throw new MatrixLengthException("Матрицы не одинаковые");
        }
        result = new int[m1.length][m1[0].length];
        for (int i = 0; i < m1.length; i++) {
            for (int j = 0; j < m1[0].length; j++) {
                result[i][j] = m1[i][j] - m2[i][j];
            }
        }
        return result;
    }

    public static int[][] matrixMultiply(Matrix mat1, Matrix mat2) throws NullPointerException, MatrixLengthException {
        int[][] result;
        int[][] m1 = mat1.getMatrix();
        int[][] m2 = mat2.getMatrix();
        if (m1 == null || m2 == null) {
            throw new NullPointerException("Матрицы пустые");
        }
        if (m1[0].length != m2.length) {
            throw new MatrixLengthException("Высота первой и ширина второй матриц не совпадают");
        }
        result = new int[m1.length][m2[0].length];
        for (int i = 0; i < m1.length; i++) {
            for (int j = 0; j < m2[0].length; j++) {
                for (int z = 0; z < m2.length; z++) {
                    result[i][j] += m1[i][z] * m2[z][j];
                }
            }
        }
        return result;
    }
}
